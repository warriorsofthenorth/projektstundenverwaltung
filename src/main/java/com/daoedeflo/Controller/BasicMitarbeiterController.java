package com.daoedeflo.Controller;

import com.daoedeflo.Checker.CheckController;
import com.daoedeflo.DAO.BasicMitarbeiterDAO;
import com.daoedeflo.DAO.MitarbeiterDAO;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public class BasicMitarbeiterController implements MitarbeiterController{

    private CheckController checkController = new CheckController();

    @Override
    public int createMitarbeiterAndReturnId(Mitarbeiter mitarbeiter) throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException, DataClassException {
        MitarbeiterDAO mitarbeiterDAO = new BasicMitarbeiterDAO();

        checkController.checkIfMitarbeiterNotEmpty(mitarbeiter);

        if( ! doesMitarbeiterExistAlready(mitarbeiter) ) {
            mitarbeiterDAO.createMitarbeiter(mitarbeiter);
        }

        Mitarbeiter findMitarbeiter = mitarbeiterDAO.readMitarbeiterByName(mitarbeiter.
                        getVorName(), mitarbeiter.getNachName());

        return findMitarbeiter.getMitarbeiterID();
    }

    private boolean doesMitarbeiterExistAlready(Mitarbeiter mitarbeiter) throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException {
        MitarbeiterDAO mitarbeiterDAO = new BasicMitarbeiterDAO();

        if(mitarbeiterDAO.readMitarbeiterByName( mitarbeiter.getVorName(),
                mitarbeiter.getNachName() ).getMitarbeiterID() != 0 )
            return true;
        else
            return false;
    }
}
