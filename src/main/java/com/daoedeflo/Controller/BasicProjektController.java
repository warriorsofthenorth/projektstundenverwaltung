package com.daoedeflo.Controller;

import com.daoedeflo.Checker.CheckController;
import com.daoedeflo.DAO.BasicMitarbeiterDAO;
import com.daoedeflo.DAO.BasicProjektDAO;
import com.daoedeflo.DAO.MitarbeiterDAO;
import com.daoedeflo.DAO.ProjektDAO;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public class BasicProjektController implements ProjektController {

    private CheckController checkController = new CheckController();

    @Override
    public Set<String> getCompleteProjektList()
            throws IDoutOfBoundsException, SQLException,
            NameOutOfBoundsException, DAOException {

        ProjektDAO projektDAO = new BasicProjektDAO();
        Set<String> projektNameList = new TreeSet<String>();

        Set<Projekt> projektList = projektDAO.readAllProjekts();

        for(Projekt projekt : projektList){
            projektNameList.add(projekt.getProjektName());
        }

        return projektNameList;
    }

    @Override
    public Set<Mitarbeiter> getMitarbeiterOfAProject(Projekt projekt) throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException, DataClassException {
        Set<Mitarbeiter> mitarbeiterList = new TreeSet<Mitarbeiter>();
        MitarbeiterDAO mitarbeiterDAO = new BasicMitarbeiterDAO();
        ProjektDAO projektDAO = new BasicProjektDAO();

        checkController.checkIfProjektNameNotEmpty(projekt);

        Set<Projekt> projektList = projektDAO.readProjektByProjektName(
                                        projekt.getProjektName());
        int mitarbeiterID=0;

        for(Projekt tempProjekt : projektList){
            mitarbeiterID = tempProjekt.getMitarbeiter().getMitarbeiterID();
            if(mitarbeiterID>0)
                mitarbeiterList.add(mitarbeiterDAO.readMitarbeiterById(mitarbeiterID));
        }

        return mitarbeiterList;
    }

    public int createProjektAndReturnId(Projekt projekt)
            throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException,
            DAOException, DataClassException {
        ProjektDAO projektDAO = new BasicProjektDAO();

        checkController.checkIfProjektNotEmpty(projekt);

        if( ! doesProjektExistAlready(projekt) )
            projektDAO.createProjekt(projekt);

        Projekt findProject = projektDAO.readProjektByMitarbeiterID(projekt
                        .getMitarbeiter().getMitarbeiterID());

        return findProject.getProjektID();
    }

    private boolean doesProjektExistAlready(Projekt projekt) throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException {
        ProjektDAO projektDAO = new BasicProjektDAO();

        if(projektDAO.readProjektByMitarbeiterID(
                projekt.getMitarbeiter().getMitarbeiterID() ).getProjektID() != 0 )
            return true;
        else
            return false;
    }
}
