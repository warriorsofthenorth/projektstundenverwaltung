package com.daoedeflo.Controller;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public interface ProjektController {
    public Set<String> getCompleteProjektList() throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException;
    public Set<Mitarbeiter> getMitarbeiterOfAProject(Projekt projekt) throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException, DataClassException;
    public int createProjektAndReturnId(Projekt projekt)
            throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException,
            DAOException, DataClassException;
}
