package com.daoedeflo.Controller;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public interface ArbeitsZeitController {
    public int createNewArbeitsZeitAndReturnId(ArbeitsZeit arbeitsZeit) throws IDoutOfBoundsException, SQLException, DateOutOfBoundsException, DAOException;
    public boolean timeCollisionWithExistingEntry(ArbeitsZeit arbeitsZeit) throws DateOutOfBoundsException, DAOException, SQLException;
    public Set<ArbeitsZeit> getArbeitsZeitList(Projekt projekt, String monthYear) throws DateOutOfBoundsException, DAOException, SQLException, DataClassException;
    public String calculateMonthlyHours(Projekt projekt, String monthYear) throws DateOutOfBoundsException, DAOException, SQLException, DataClassException;
}
