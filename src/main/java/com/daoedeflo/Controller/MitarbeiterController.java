package com.daoedeflo.Controller;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public interface MitarbeiterController {
    public int createMitarbeiterAndReturnId(Mitarbeiter mitarbeiter) throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException, DataClassException;
}
