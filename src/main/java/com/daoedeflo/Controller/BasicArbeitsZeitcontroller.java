package com.daoedeflo.Controller;

import com.daoedeflo.Checker.CheckController;
import com.daoedeflo.DAO.ArbeitsZeitDAO;
import com.daoedeflo.DAO.BasicArbeitsZeitDAO;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.*;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public class BasicArbeitsZeitcontroller implements ArbeitsZeitController{

    private CheckController checkController = new CheckController();

    @Override
    public int createNewArbeitsZeitAndReturnId(ArbeitsZeit arbeitsZeit) throws IDoutOfBoundsException, SQLException, DateOutOfBoundsException, DAOException {

        ArbeitsZeitDAO arbeitsZeitDAO = new BasicArbeitsZeitDAO();

        checkController.checkIfArbeitsZeitNotEmpty(arbeitsZeit);

        return arbeitsZeitDAO.createArbeitsZeitAndReturnId(arbeitsZeit);

    }

    @Override
    public boolean timeCollisionWithExistingEntry(ArbeitsZeit arbeitsZeit)
            throws DAOException, SQLException, DateOutOfBoundsException {
        ArbeitsZeitDAO zeitenDAO = new BasicArbeitsZeitDAO();
        Set<ArbeitsZeit> zeitenListe = new HashSet<ArbeitsZeit>();
        boolean collisionStatus = false;

        zeitenListe = zeitenDAO.readArbeitsZeitByDateAndMitarbeiterID(
                arbeitsZeit.getMitarbeiterID(), arbeitsZeit.getDatum());

        for(ArbeitsZeit zeit : zeitenListe){
            if(collisionChecker(zeit,arbeitsZeit) == true)
                collisionStatus = true;
        }
        return collisionStatus;
    }

    @Override
    public Set<ArbeitsZeit> getArbeitsZeitList(Projekt projekt, String monthYear) throws DateOutOfBoundsException, DAOException, SQLException, DataClassException {
        checkController.checkIfProjektNotEmpty(projekt);
        checkController.checkMonthYear(monthYear);

        ArbeitsZeitDAO arbeitsZeitDAO = new BasicArbeitsZeitDAO();
        Set<ArbeitsZeit> arbeitsDaySet = new LinkedHashSet<>();
        Set<ArbeitsZeit> arbeitsMonthSet = new LinkedHashSet<>();
        int mitarbeiterID = projekt.getMitarbeiter().getMitarbeiterID();
        Calendar datum = monthYearToDate(monthYear);
        int day;

        for(day=1;day<=31;day++){
            datum = changeDay(datum,day);
            arbeitsDaySet = arbeitsZeitDAO
                    .readArbeitsZeitByDateAndMitarbeiterID(mitarbeiterID,datum);
            for(ArbeitsZeit arbeitsDay : arbeitsDaySet){
                arbeitsMonthSet.add(arbeitsDay);
            }
        }


        return arbeitsMonthSet;
    }

    @Override
    public String calculateMonthlyHours(Projekt projekt, String monthYear) throws DateOutOfBoundsException, DAOException, SQLException, DataClassException {
        Set<ArbeitsZeit> arbeitsMonthSet = new LinkedHashSet<ArbeitsZeit>();
        arbeitsMonthSet = this.getArbeitsZeitList(projekt,monthYear);
        int totalMinutes = 0;

        for(ArbeitsZeit arbeitsZeit : arbeitsMonthSet){
            totalMinutes += calculateSessionTime(arbeitsZeit.getStartZeit(),
                                                arbeitsZeit.getEndZeit());
        }

        return minutesToTimeString(totalMinutes);
    }

    private String minutesToTimeString(int minutes){
        String timeString = "";
        int hours = minutes/60;
        minutes = minutes % 60;
        timeString = hours + ":" + minutes;
        return timeString;
    }

    private int calculateSessionTime(LocalTime startZeit, LocalTime endZeit){
        int startMinutes = 0;
        int endMinutes = 0;
        startMinutes = localTimeToIntegerMinutes(startZeit);
        endMinutes = localTimeToIntegerMinutes(endZeit);

        return (endMinutes - startMinutes) ;
    }

    private int localTimeToIntegerMinutes(LocalTime time){
        return ( time.getHour() * 60 ) + time.getMinute();
    }


    private Calendar changeDay(Calendar date, int newDay){
        date.set(date.get(Calendar.YEAR),date.get(Calendar.MONTH),newDay);
        return date;
    }

    private Calendar monthYearToDate(String monthYear){

        String monthYearSplit[] = monthYear.split("-");
        int year = Integer.parseInt(monthYearSplit[0]);
        int month = Integer.parseInt(monthYearSplit[1]) - 1;      //because of
                                                                //Calendar fail
        Calendar datum = new GregorianCalendar(year, month, 1);  //Month minus one
                                                                //on write
        return datum;
    }

    private boolean collisionChecker(ArbeitsZeit arbeitsZeit, ArbeitsZeit neueArbeitsZeit)
            throws DateOutOfBoundsException {

        if (neueArbeitsZeitIsOverArbeitsZeit(arbeitsZeit, neueArbeitsZeit)) {
            return true;
        }

        if (neueArbeitsZeitIsInsideArbeitsZeit(neueArbeitsZeit.getStartZeit(),
                arbeitsZeit)){
            return true;
        }

        if(neueArbeitsZeitIsInsideArbeitsZeit(neueArbeitsZeit.getEndZeit(),
                arbeitsZeit)) {
            return true;
        }

        return false;
    }

    private boolean neueArbeitsZeitIsInsideArbeitsZeit(LocalTime zeit,
                                                       ArbeitsZeit arbeitsZeit){
        int neueZeit = ( zeit.getHour() * 100 ) + zeit.getMinute();
        int alteStartZeit = ( arbeitsZeit.getStartZeit().getHour() * 100 ) +
                arbeitsZeit.getStartZeit().getMinute();
        int alteEndZeit = ( arbeitsZeit.getEndZeit().getHour() * 100 ) +
                arbeitsZeit.getEndZeit().getMinute();


        if( (neueZeit > alteStartZeit)  && (neueZeit < alteEndZeit) )
            return true;

        if( neueZeit == alteStartZeit)
            return true;

        if( neueZeit == alteEndZeit)
            return true;


        return false;
    }

    private boolean neueArbeitsZeitIsOverArbeitsZeit(ArbeitsZeit aZeit,
                                                     ArbeitsZeit neueAZeit){
        int neueStartZeit = ( neueAZeit.getStartZeit().getHour() * 100 ) +
                neueAZeit.getStartZeit().getMinute();
        int neueEndZeit = ( neueAZeit.getEndZeit().getHour() * 100 ) +
                neueAZeit.getEndZeit().getMinute();
        int alteStartZeit = ( aZeit.getStartZeit().getHour() * 100 ) +
                aZeit.getStartZeit().getMinute();
        int alteEndZeit = (aZeit.getEndZeit().getHour() * 100 ) +
                aZeit.getEndZeit().getMinute();

        if( (neueStartZeit < alteStartZeit) && (neueEndZeit > alteEndZeit) )
            return true;

        return false;
    }
}
