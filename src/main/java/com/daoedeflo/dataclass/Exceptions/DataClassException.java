package com.daoedeflo.dataclass.Exceptions;

/**
 * Created by Florian Mitterbauer on 1/21/15.
 */
public class DataClassException extends Exception {
    public DataClassException(){
        super();
    }
    public DataClassException(String message){
        super(message);
    }
    public DataClassException(Throwable cause){
        super(cause);
    }
    public DataClassException(Throwable cause, String message){
        super(message,cause);
    }

}
