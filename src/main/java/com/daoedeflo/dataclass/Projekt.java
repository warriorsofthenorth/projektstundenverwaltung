package com.daoedeflo.dataclass;

import com.daoedeflo.Checker.CheckDAO;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;

/**
 * Created by Florian Mitterbauer on 1/13/15.
 */
public class Projekt {
    private int projektID;
    private Mitarbeiter mitarbeiter;
    private String projektName;

    private CheckDAO checkDAO = new CheckDAO();


    public Projekt() {
        this.mitarbeiter = new Mitarbeiter();
        this.projektName="";
    }

    public int getProjektID() {
        return projektID;
    }

    public void setProjektID(int projektID)
        throws IDoutOfBoundsException{

        checkDAO.checkID(projektID,"Projekt");
        this.projektID = projektID;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter)
        throws IDoutOfBoundsException{

        this.mitarbeiter = mitarbeiter;
    }

    public String getProjektName() {
        return projektName;
    }

    public void setProjektName(String projektName)
        throws NameOutOfBoundsException{

        checkDAO.checkName(projektName,"Projekt Name");
        this.projektName = projektName;
    }
}
