package com.daoedeflo.dataclass;

import com.daoedeflo.Checker.CheckDAO;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;

import java.time.LocalTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Florian Mitterbauer on 1/13/15.
 */
public class ArbeitsZeit {
    private int arbeitsZeitID;
    private int mitarbeiterID;
    private int projektID;
    private Calendar datum;
    private LocalTime startZeit;
    private LocalTime endZeit;

    private CheckDAO checkArbeitsZeit= new CheckDAO();


    public ArbeitsZeit() {
        this.arbeitsZeitID=0;
        this.projektID=0;
        this.arbeitsZeitID=0;
        this.datum = new GregorianCalendar();
        this.startZeit = LocalTime.of(0,0);
        this.endZeit = LocalTime.of(0,0);
    }


    public int getArbeitsZeitID() {
        return arbeitsZeitID;
    }

    public void setArbeitsZeitID(int arbeitsZeitID)
        throws IDoutOfBoundsException{
        checkArbeitsZeit.checkID(arbeitsZeitID,"ArbeitsZeit");
        this.arbeitsZeitID = arbeitsZeitID;
    }

    public int getMitarbeiterID() {
        return mitarbeiterID;
    }

    public void setMitarbeiterID(int mitarbeiterID)
        throws IDoutOfBoundsException{
        checkArbeitsZeit.checkID(mitarbeiterID,"Mitarbeiter");
        this.mitarbeiterID = mitarbeiterID;
    }

    public int getProjektID() {
        return projektID;
    }

    public void setProjektID(int projektID)
        throws IDoutOfBoundsException{
        checkArbeitsZeit.checkID(projektID,"Projekt");
        this.projektID = projektID;
    }

    public Calendar getDatum() {
        return datum;
    }

    public void setDatum(Calendar datum)
        throws DateOutOfBoundsException{
        checkArbeitsZeit.checkDatum(datum);
        this.datum = datum;
    }

    public LocalTime getStartZeit() {
        return startZeit;
    }

    public void setStartZeit(LocalTime startZeit)
            throws DateOutOfBoundsException{
        checkArbeitsZeit.checkStartZeit(startZeit,this.endZeit);
        this.startZeit = startZeit;
    }

    public LocalTime getEndZeit() {
        return endZeit;
    }

    public void setEndZeit(LocalTime endZeit)
            throws DateOutOfBoundsException{
        checkArbeitsZeit.checkEndZeit(this.startZeit,endZeit);
        this.endZeit = endZeit;
    }
}
