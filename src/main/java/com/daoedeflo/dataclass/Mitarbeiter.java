package com.daoedeflo.dataclass;

import com.daoedeflo.Checker.CheckDAO;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/13/15.
 */
public class Mitarbeiter implements Comparable<Mitarbeiter>{
    private String vorName;
    private String nachName;
    private Set<ArbeitsZeit> arbeitsZeitenList;
    private int mitarbeiterID;

    private CheckDAO checkDAO = new CheckDAO();

    public Mitarbeiter(){
        this.vorName="";
        this.nachName="";
        this.mitarbeiterID=0;
        this.arbeitsZeitenList = new HashSet<ArbeitsZeit>();
    }


    public int getMitarbeiterID() {
        return mitarbeiterID;
    }

    public void setMitarbeiterID(int mitarbeiterID)
        throws IDoutOfBoundsException{

        checkDAO.checkID(mitarbeiterID,"Mitarbeiter");
        this.mitarbeiterID = mitarbeiterID;
    }

    public String getVorName() {
        return vorName;
    }

    public void setVorName(String vorName)
        throws NameOutOfBoundsException{

        checkDAO.checkName(vorName,"VorName");
        this.vorName = vorName;
    }

    public String getNachName() {
        return nachName;
    }

    public void setNachName(String nachName)
        throws NameOutOfBoundsException{

        checkDAO.checkName(nachName,"NachName");
        this.nachName = nachName;
    }


    public Set<ArbeitsZeit> getArbeitsZeitenList() {
        return arbeitsZeitenList;
    }

    public void setArbeitsZeitenList(Set<ArbeitsZeit> arbeitsZeitenList) {
        this.arbeitsZeitenList = arbeitsZeitenList;
    }

    @Override
    public int compareTo(Mitarbeiter o) {
        int nachNameCompare = this.getNachName().compareTo(o.getNachName());
        if(nachNameCompare != 0){
            return nachNameCompare;
        }else{
            return this.getVorName().compareTo(o.getVorName());
        }
    }
}
