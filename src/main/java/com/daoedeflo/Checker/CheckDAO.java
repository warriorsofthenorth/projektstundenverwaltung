package com.daoedeflo.Checker;

import com.daoedeflo.DAO.ArbeitsZeitDAO;
import com.daoedeflo.DAO.BasicArbeitsZeitDAO;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.*;

/**
 * Created by Florian Mitterbauer on 1/16/15.
 */
public class CheckDAO {
    private static final int NAME_LENGTH_MAX = 50;
    private static final int NAME_LENGTH_MIN = 1;
    private static final int ID_MAX = 1000;
    private static final int ID_MIN = 1;
    private static final int YEAR_MIN = 1971;


    public void checkDatum(Calendar datum)
            throws DateOutOfBoundsException{

        Calendar heute = new GregorianCalendar();
        int heutigerTag = heute.get(Calendar.DAY_OF_MONTH);
        if(datum.get(Calendar.YEAR)<YEAR_MIN)
            throw new DateOutOfBoundsException("Date to long ago!");
        //if(datum.get(Calendar.DAY_OF_MONTH)>heutigerTag)
        //    throw new DateOutOfBoundsException("Future Dates not valid!");
    }


    public void checkStartZeit(LocalTime startZeit, LocalTime endZeit)
                throws DateOutOfBoundsException{
        if(startZeit.equals(LocalTime.of(23,59)))
            throw new DateOutOfBoundsException("StartZeit vom Tag kann nicht " +
                    "23:59 sein!");
        if(isEndZeitAfterStartZeit(startZeit,endZeit)){
            throw new DateOutOfBoundsException("EndZeit kann nicht vor StartZeit sein!");
        }
        if(startZeit.equals(endZeit))
            throw new DateOutOfBoundsException("StartZeit kann nicht gleich " +
                    "EndZeit sein!");
    }


    public void checkEndZeit(LocalTime startZeit, LocalTime endZeit)
            throws DateOutOfBoundsException{
        if(endZeit.equals(LocalTime.of(0,0)))
            throw new DateOutOfBoundsException("EndZeit vom Tag kann nicht 0:00 sein!");
        if(isEndZeitAfterStartZeit(startZeit,endZeit)){
            throw new DateOutOfBoundsException("EndZeit kann nicht vor StartZeit sein!");
        }
        if(startZeit.equals(endZeit))
            throw new DateOutOfBoundsException("EndZeit kann nicht gleich " +
                    "StartZeit sein!");
    }

    private boolean isEndZeitAfterStartZeit(LocalTime startZeit, LocalTime endZeit){
        if( (endZeit.getHour()==0)  &&   (endZeit.getMinute()==0) )
            return false;

        if( startZeit.getHour() > endZeit.getHour() ) {
            return true;
        }
        if( (startZeit.getHour()==endZeit.getHour())  &&
                startZeit.getMinute() > endZeit.getMinute() ) {
            return true;
        }
        return false;
    }

    public void checkID(int id, String idName)
            throws IDoutOfBoundsException{
        if(id<ID_MIN)
            throw new IDoutOfBoundsException(idName + "ID < "+ID_MIN+" not valid");
        if(id>ID_MAX)
            throw new IDoutOfBoundsException(idName + "ID > "+ID_MAX+" not valid");
    }

    public void checkName(String value, String keyName)
            throws NameOutOfBoundsException{
        if(value.length()<NAME_LENGTH_MIN)
            throw new NameOutOfBoundsException(keyName + " to Short");
        if(value.length()>NAME_LENGTH_MAX)
            throw new NameOutOfBoundsException(keyName + " to Long");
    }

    public void checkForTimeCollision(ArbeitsZeit neueArbeitsZeit)
            throws DateOutOfBoundsException, DAOException, SQLException {
        ArbeitsZeitDAO zeitenDAO = new BasicArbeitsZeitDAO();
        Set<ArbeitsZeit> zeitenListe = new HashSet<ArbeitsZeit>();
        zeitenListe = zeitenDAO.readArbeitsZeitByDateAndMitarbeiterID(
                neueArbeitsZeit.getMitarbeiterID(), neueArbeitsZeit.getDatum());

        for(ArbeitsZeit zeit : zeitenListe){
            collisionChecker(zeit,neueArbeitsZeit);
        }

    }

    private void collisionChecker(ArbeitsZeit arbeitsZeit, ArbeitsZeit neueArbeitsZeit)
            throws DateOutOfBoundsException {

        if(neueArbeitsZeitIsOverArbeitsZeit(arbeitsZeit,neueArbeitsZeit))
            throw new DateOutOfBoundsException("Time collision new Time over old Time");

        if(neueArbeitsZeitIsInsideArbeitsZeit(neueArbeitsZeit.getStartZeit(),
                arbeitsZeit))
            throw new DateOutOfBoundsException("Time Collision StartZeit inside " +
                    "another Time Entry");

        if(neueArbeitsZeitIsInsideArbeitsZeit(neueArbeitsZeit.getEndZeit(),
                arbeitsZeit))
            throw new DateOutOfBoundsException("Time Collision EndZeit inside " +
                    "another Time Entry");



    }

    private boolean neueArbeitsZeitIsInsideArbeitsZeit(LocalTime zeit,
                                                       ArbeitsZeit arbeitsZeit){
        int neueZeit = ( zeit.getHour() * 100 ) + zeit.getMinute();
        int alteStartZeit = ( arbeitsZeit.getStartZeit().getHour() * 100 ) +
                                arbeitsZeit.getStartZeit().getMinute();
        int alteEndZeit = ( arbeitsZeit.getEndZeit().getHour() * 100 ) +
                                arbeitsZeit.getEndZeit().getMinute();


        if( (neueZeit > alteStartZeit)  && (neueZeit < alteEndZeit) )
            return true;

        if( neueZeit == alteStartZeit)
            return true;

        if( neueZeit == alteEndZeit)
            return true;


        return false;
    }

    private boolean neueArbeitsZeitIsOverArbeitsZeit(ArbeitsZeit aZeit,
                                                     ArbeitsZeit neueAZeit){
        int neueStartZeit = ( neueAZeit.getStartZeit().getHour() * 100 ) +
                                neueAZeit.getStartZeit().getMinute();
        int neueEndZeit = ( neueAZeit.getEndZeit().getHour() * 100 ) +
                                neueAZeit.getEndZeit().getMinute();
        int alteStartZeit = ( aZeit.getStartZeit().getHour() * 100 ) +
                                aZeit.getStartZeit().getMinute();
        int alteEndZeit = (aZeit.getEndZeit().getHour() * 100 ) +
                                aZeit.getEndZeit().getMinute();

        if( (neueStartZeit < alteStartZeit) && (neueEndZeit > alteEndZeit) )
            return true;

        return false;
    }

    public void checkIfProjektNotEmpty(Projekt projekt)
            throws DAOException{
        this.checkIfProjektNameNotEmpty(projekt);
        if(projekt.getMitarbeiter().getMitarbeiterID()==0)
            throw new DAOException("No MitarbeiterID in Projekt defined!");
    }

    public void checkIfMitarbeiterNotEmpty(Mitarbeiter mitarbeiter)
            throws DAOException {
        if(mitarbeiter.getVorName().equals(""))
            throw new DAOException("No VorName in Mitarbeiter defined!");
        if(mitarbeiter.getNachName().equals(""))
            throw new DAOException("No NachName in Mitarbeiter defined!");
    }

    private void checkIfProjektNameNotEmpty(Projekt projekt)
            throws DAOException{
        if (projekt.getProjektName().equals(""))
            throw new DAOException("No ProjektName in Projekt defined!");
    }

    public void checkIfArbeitsZeitNotEmpty(ArbeitsZeit arbeitsZeit) throws IDoutOfBoundsException, DateOutOfBoundsException {
        this.checkID(arbeitsZeit.getMitarbeiterID(), "Mitarbeiter");
        this.checkID(arbeitsZeit.getProjektID(),"Projekt");
        this.checkDatum(arbeitsZeit.getDatum());
        this.checkStartZeit( arbeitsZeit.getStartZeit(), arbeitsZeit.getEndZeit() );
        this.checkEndZeit( arbeitsZeit.getStartZeit(), arbeitsZeit.getEndZeit() );
    }



}
