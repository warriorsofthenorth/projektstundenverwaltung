package com.daoedeflo.Checker;

import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import sun.util.calendar.Gregorian;

import java.time.LocalTime;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public class CheckGUI {

    private static final int NAME_LENGTH_MAX = 50;
    private static final int NAME_LENGTH_MIN = 1;
    private static final String NOT_VALID = "";

    public int checkIfMainMenuEntryIsValidAndReturn(String menuItem){
        try{
            int menuNumber = Integer.parseInt(menuItem);
            if( menuNumber >= 1 && menuNumber <= 3 )
                return menuNumber;
        }catch (NumberFormatException e){
            return 0;
        }
        return 0;
    }
    
    public int checkIfMenuEntryIsValidAndReturn(String projektName,
                                                       int maxValue){
        try{
            int menuNumber = Integer.parseInt(projektName);
            if( menuNumber >= 1 && menuNumber <= maxValue )
                return menuNumber;
        }catch (NumberFormatException e){
            return 0;
        }
        return 0;
    }

    public String checkIfNameEntryIsValidAndReturn(String name){
        name = filterName(name);
        name = name.trim();

        if(name.length() > NAME_LENGTH_MAX)
            name = NOT_VALID;
        if(name.length() < NAME_LENGTH_MIN)
            name = NOT_VALID;

        return name;
    }

    public String checkIfStartTimeEntryIsValidAndReturn(String time){
        time = filterTime(time);
        if( ! time.equals("") ){
            String[] timeSplit = time.split(":");
            if ( Integer.parseInt(timeSplit[0]) == 23 &&
                    Integer.parseInt(timeSplit[1]) == 59 ) {
                time = NOT_VALID;
            }
        }
        return time;
    }

    public String checkIfEndTimeEntryIsValidAndReturn(String time, String startTime) throws DateOutOfBoundsException {

        time = filterTime(time);
        startTime = filterTime(startTime);

        if( (! time.equals("")) && (! startTime.equals("")) ){
            String[] timeSplit = time.split(":");
            String[] startTimeSplit = startTime.split(":");
            LocalTime endZeit = LocalTime.of(Integer.parseInt(timeSplit[0]),
                                            Integer.parseInt(timeSplit[1]));
            LocalTime startZeit = LocalTime.of(Integer.parseInt(startTimeSplit[0]),
                                            Integer.parseInt(startTimeSplit[1]));
            if(endZeitIsNOTvalidToStartZeit(startZeit,endZeit)){
                return NOT_VALID;
            }
        }
        return time;
    }

    private boolean endZeitIsNOTvalidToStartZeit(LocalTime startZeit, LocalTime endZeit)
            throws DateOutOfBoundsException{
        if(endZeit.equals(LocalTime.of(0, 0))){
            return true;
        }
        if(isEndZeitAfterStartZeit(startZeit,endZeit)){
            return true;
        }
        if(startZeit.equals(endZeit)){
            return true;
        }

        return false;
    }

    private boolean isEndZeitAfterStartZeit(LocalTime startZeit, LocalTime endZeit){
        if( (endZeit.getHour()==0)  &&   (endZeit.getMinute()==0) )
            return false;

        if( startZeit.getHour() > endZeit.getHour() ) {
            return true;
        }
        if( (startZeit.getHour()==endZeit.getHour())  &&
                startZeit.getMinute() > endZeit.getMinute() ) {
            return true;
        }
        return false;
    }

    private String filterTime(String timeString){

        if(timeString.matches("(([0-1][0-9])|([2][0-3])):([0-5][0-9])"))
            return timeString;
        else
            return "";

    }

    public String checkIfDateEntryIsValidAndReturn(String date){

        date = filterdate(date);
        if( ! date.equals("") ){
            String[] datumSplittedString = date.split("-");
            if( Integer.parseInt(datumSplittedString[0]) < 1971)
                date = "";
        }

        return date;
    }

    public String checkIfYearMonthEntryIsValidAndReturn(String date){
        date = filterYearAndMonth(date);
        if( ! date.equals("") ){
            String[] datumSplittedString = date.split("-");
            if( Integer.parseInt(datumSplittedString[0]) < 1971)
                date = "";
        }
        return date;
    }

    private String filterYearAndMonth(String yearMonth){
        if(yearMonth.matches(
                "^((19|20)\\d\\d)-(0?[1-9]|1[012])$") ){
            return yearMonth;
        }else{
            return "";
        }
    }


    private String filterdate(String dateString){
        if(dateString.matches(
             "^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$") ){
            return dateString;
        }else{
            return "";
        }

    }

    private String filterName(String name){
        return name.replaceAll("[^\\w]","");
    }
}
