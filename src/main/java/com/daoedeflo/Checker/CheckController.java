package com.daoedeflo.Checker;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;

/**
 * Created by Florian Mitterbauer on 1/21/15.
 */
public class CheckController {
    public void checkIfProjektNotEmpty(Projekt projekt)
        throws DataClassException{
        this.checkIfProjektNameNotEmpty(projekt);
        if(projekt.getMitarbeiter().getMitarbeiterID()==0)
            throw new DataClassException("No MitarbeiterID in Projekt defined!");
    }

    public void checkIfMitarbeiterNotEmpty(Mitarbeiter mitarbeiter)
            throws DataClassException {
        if(mitarbeiter.getVorName().equals(""))
            throw new DataClassException("No VorName in Mitarbeiter defined!");
        if(mitarbeiter.getNachName().equals(""))
            throw new DataClassException("No NachName in Mitarbeiter defined!");
    }

    public void checkIfProjektNameNotEmpty(Projekt projekt)
            throws DataClassException{
        if (projekt.getProjektName().equals(""))
            throw new DataClassException("No ProjektName in Projekt defined!");
    }

    public void checkIfArbeitsZeitNotEmpty(ArbeitsZeit arbeitsZeit) throws IDoutOfBoundsException, DateOutOfBoundsException, SQLException, DAOException {
        CheckDAO checkDAO = new CheckDAO();
        checkDAO.checkIfArbeitsZeitNotEmpty(arbeitsZeit);
        checkDAO.checkForTimeCollision(arbeitsZeit);
    }

    public void checkMonthYear(String monthYear)
        throws DataClassException{
        monthYear = filterYearAndMonth(monthYear);
        if( ! monthYear.equals("") ){
            String[] datumSplittedString = monthYear.split("-");
            if( Integer.parseInt(datumSplittedString[0]) < 1971)
                throw new DataClassException("Time to long ago!!");
        }


    }

    private String filterYearAndMonth(String yearMonth)
        throws DataClassException{
        if(yearMonth.matches(
                "^((19|20)\\d\\d)-(0?[1-9]|1[012])$") ){
            return yearMonth;
        }else{
            throw new DataClassException("yearMonth not Valid, should be YYYY-MM !!");
        }
    }


}
