package com.daoedeflo.DAO;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Mitarbeiter;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/13/15.
 */
public interface ArbeitsZeitDAO {
    //public int createArbeitsZeit(int mitarbeiterID, int projektID, Calendar datum,
    //                             LocalTime startZeit, LocalTime endZeit)
    public int createArbeitsZeitAndReturnId(ArbeitsZeit arbeitsZeitToCreate)
            throws SQLException, DAOException, IDoutOfBoundsException,
                    DateOutOfBoundsException;
    public Set<ArbeitsZeit> readArbeitsZeitByDateAndMitarbeiterID(int mitarbeiterID,
                                                                Calendar datum)
            throws  DateOutOfBoundsException, SQLException, DAOException;
    public void deleteArbeitsZeit(ArbeitsZeit arbeitsZeitToDelete)
            throws SQLException, DAOException;
}
