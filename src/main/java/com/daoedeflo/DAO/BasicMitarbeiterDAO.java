package com.daoedeflo.DAO;

import com.daoedeflo.Checker.CheckDAO;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Mitarbeiter;

import java.sql.*;
import java.util.Properties;

/**
 * Created by Florian Mitterbauer on 1/13/15.
 */
public class BasicMitarbeiterDAO implements MitarbeiterDAO {
    private ConnectorDAO connectorDAO = new BasicConnectorDAO();
    private CheckDAO checkDAO = new CheckDAO();


    @Override
    public void createMitarbeiter(Mitarbeiter mitarbeiter)
            throws DAOException, NameOutOfBoundsException, SQLException,
            IDoutOfBoundsException {
        if(this.readMitarbeiterByName(mitarbeiter.getVorName(),
                mitarbeiter.getNachName()).getMitarbeiterID()!=0)
            throw new DAOException("Mitarbeiter Double Entries (Same Name) not allowed!!");
        checkDAO.checkIfMitarbeiterNotEmpty(mitarbeiter);

        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String createSQL ="INSERT INTO mitarbeiter values(?,?,?)";

        try{
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(createSQL);
            stmnt.setInt(1, connectorDAO.getNextID(TableNames.MITARBEITER));
            stmnt.setString(2, mitarbeiter.getVorName());
            stmnt.setString(3, mitarbeiter.getNachName());
            stmnt.executeUpdate();
            con.commit();
        }catch(SQLException e){
            System.out.print("SQL InsertMitarbeiter Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
    }


    @Override
    public Mitarbeiter readMitarbeiterByName(String vorName, String nachName)
            throws NameOutOfBoundsException, SQLException, DAOException,
            IDoutOfBoundsException {
        checkDAO.checkName(vorName, "VorName");
        checkDAO.checkName(nachName,"NachName");

        Mitarbeiter readMitarbeiter = new Mitarbeiter();
        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String readSQL = "SELECT * FROM mitarbeiter WHERE vorname = ? AND nachname = ? ";

        try{
            stmnt = con.prepareStatement(readSQL);
            stmnt.setString(1,vorName);
            stmnt.setString(2,nachName);

            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return readMitarbeiter;

            while(results.next()){
                readMitarbeiter.setMitarbeiterID(results.getInt("id"));
                readMitarbeiter.setVorName(results.getString("vorname"));
                readMitarbeiter.setNachName(results.getString("nachname"));
            }
        }catch(SQLException e){
            System.out.print("SQL InsertMitarbeiter Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
        return readMitarbeiter;
    }

    @Override
    public Mitarbeiter readMitarbeiterById(int mitarbeiterID)
            throws SQLException, DAOException, IDoutOfBoundsException,
            NameOutOfBoundsException {
        checkDAO.checkID(mitarbeiterID,"Mitarbeiter");

        Mitarbeiter readMitarbeiter = new Mitarbeiter();
        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String readSQL = "SELECT * FROM mitarbeiter WHERE id = ? ";

        try{
            stmnt = con.prepareStatement(readSQL);
            stmnt.setInt(1,mitarbeiterID);

            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return readMitarbeiter;

            while(results.next()){
                readMitarbeiter.setMitarbeiterID(results.getInt("id"));
                readMitarbeiter.setVorName(results.getString("vorname"));
                readMitarbeiter.setNachName(results.getString("nachname"));
            }
        }catch(SQLException e){
            System.out.print("SQL InsertMitarbeiter Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
        return readMitarbeiter;
    }


    @Override
    public void deleteMitarbeiter(Mitarbeiter mitarbeiterToDelete)
            throws SQLException, DAOException {
        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String deleteSQL ="DELETE FROM mitarbeiter WHERE vorname = ? " +
                                                    "AND nachname = ? ";
        try{
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(deleteSQL);
            stmnt.setString(1, mitarbeiterToDelete.getVorName());
            stmnt.setString(2, mitarbeiterToDelete.getNachName());
            stmnt.executeUpdate();
            con.commit();
        }catch(SQLException e){
            System.out.print("SQL DeleteMitarbeiter Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
    }

}
