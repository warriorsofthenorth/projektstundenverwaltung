package com.daoedeflo.DAO;

import com.daoedeflo.Checker.CheckDAO;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Mitarbeiter;

import java.sql.*;
import java.time.LocalTime;
import java.util.*;

/**
 * Created by Florian Mitterbauer on 1/16/15.
 */
public class BasicArbeitsZeitDAO implements ArbeitsZeitDAO {

    private ConnectorDAO connectorDAO = new BasicConnectorDAO();
    private CheckDAO checkArbeitsZeit = new CheckDAO();

    @Override
    public int createArbeitsZeitAndReturnId(ArbeitsZeit arbeitsZeitToCreate)
            throws SQLException, DAOException, IDoutOfBoundsException,
            DateOutOfBoundsException {
        checkArbeitsZeit.checkIfArbeitsZeitNotEmpty(arbeitsZeitToCreate);
        checkArbeitsZeit.checkForTimeCollision(arbeitsZeitToCreate);

        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String createSQL = "INSERT INTO arbeitszeit values(?,?,?,?,?,?)";
        int Aid = connectorDAO.getNextID(TableNames.ARBEITSZEIT);

        try{
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(createSQL);
            stmnt.setInt(1, Aid);
            stmnt.setInt(2, arbeitsZeitToCreate.getMitarbeiterID());
            stmnt.setInt(3, arbeitsZeitToCreate.getProjektID());
            stmnt.setString(4, datumToString(arbeitsZeitToCreate.getDatum()));
            stmnt.setString(5, zeitToString( arbeitsZeitToCreate.getStartZeit() ));
            stmnt.setString(6, zeitToString( arbeitsZeitToCreate.getEndZeit() ));
            stmnt.executeUpdate();
            con.commit();

        }catch(Exception e){
            System.out.print("SQL InsertArbeitsZeit Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
        return Aid;
    }

    private Calendar stringToDaum(String datumString){
        Calendar datum = new GregorianCalendar();
        String[] datumSplittedString = datumString.split("-");
        datum.set( Integer.parseInt(datumSplittedString[0]),
                Integer.parseInt(datumSplittedString[1]) ,
                Integer.parseInt(datumSplittedString[2]) );
        return datum;
    }



    private LocalTime stringToZeit(String zeitString){
        String[] zeitSplittedString = zeitString.split(":");
        LocalTime zeit = LocalTime.of(Integer.parseInt(zeitSplittedString[0])
                ,Integer.parseInt(zeitSplittedString[1]));
        return zeit;
    }

    private String datumToString(Calendar datum){
        String formDatum = "";
        formDatum = datum.get(Calendar.YEAR) + "-"
                + datum.get(Calendar.MONTH)
                    + "-" + datum.get(Calendar.DAY_OF_MONTH);
        return formDatum;
    }


    private String zeitToString(LocalTime zeit){
        String zeitString = "";
        zeitString = zeit.getHour() + ":" + zeit.getMinute();
        return zeitString;
    }




    @Override
    public Set<ArbeitsZeit> readArbeitsZeitByDateAndMitarbeiterID(int mitarbeiterID, Calendar datum )
            throws DateOutOfBoundsException,
            SQLException, DAOException {
        checkArbeitsZeit.checkDatum(datum);

        Set<ArbeitsZeit> arbeitsZeitSet = new LinkedHashSet<ArbeitsZeit>();
        ArbeitsZeit arbeitsZeit = new ArbeitsZeit();
        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String readSQL="SELECT * FROM arbeitszeit WHERE id = ? AND datum = ? ";

        try{
            stmnt = con.prepareStatement(readSQL);
            stmnt.setInt(1,mitarbeiterID);
            stmnt.setString(2, datumToString(datum) );

            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return arbeitsZeitSet;

            while(results.next()){
                arbeitsZeit = new ArbeitsZeit();

                arbeitsZeit.setArbeitsZeitID(results.getInt("Aid"));
                arbeitsZeit.setMitarbeiterID(results.getInt("id"));
                arbeitsZeit.setProjektID(results.getInt("Pid"));
                arbeitsZeit.setDatum(stringToDaum(results.getString("datum") ) );
                arbeitsZeit.setEndZeit(stringToZeit(results.getString("endzeit")));
                arbeitsZeit.setStartZeit(stringToZeit(results.getString("startzeit")));

                arbeitsZeitSet.add(arbeitsZeit);
            }
        }catch(Exception e){
            System.out.print("SQL InsertArbeitsZeit Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }

        return arbeitsZeitSet;
    }

    @Override
    public void deleteArbeitsZeit(ArbeitsZeit arbeitsZeitToDelete)
            throws SQLException, DAOException {
        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String deleteSQL = "DELETE FROM arbeitszeit WHERE id = ? " +
                "AND Pid = ? AND datum = ? ";
        try{
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(deleteSQL);
            stmnt.setInt(1, arbeitsZeitToDelete.getMitarbeiterID());
            stmnt.setInt(2, arbeitsZeitToDelete.getProjektID());
            stmnt.setString(3, datumToString(arbeitsZeitToDelete.getDatum() ) );
            stmnt.executeUpdate();
            con.commit();
        }catch(SQLException e){
            System.out.print("SQL DeleteArbeitsZeit Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }

    }

}
