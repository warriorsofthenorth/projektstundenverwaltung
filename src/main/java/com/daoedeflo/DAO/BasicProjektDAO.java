package com.daoedeflo.DAO;

import com.daoedeflo.Checker.CheckDAO;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Projekt;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/16/15.
 */
public class BasicProjektDAO implements ProjektDAO {

    private ConnectorDAO connectorDAO = new BasicConnectorDAO();
    private CheckDAO checkDAO = new CheckDAO();

    @Override
    public void createProjekt(Projekt newProjekt)
            throws DAOException,
            SQLException, IDoutOfBoundsException, NameOutOfBoundsException {

        int mitarbeiterID = newProjekt.getMitarbeiter().getMitarbeiterID();
        String projektName = newProjekt.getProjektName();

        if(this.readProjektByMitarbeiterID(mitarbeiterID).getProjektID()!=0)
            throw new DAOException("Projekt Double Entries (Same Name) not allowed!!");
        checkDAO.checkIfProjektNotEmpty(newProjekt);

        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String createSQL ="INSERT INTO projekt values (?,?,?)";
        try{
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(createSQL);
            stmnt.setInt(1, connectorDAO.getNextID(TableNames.PROJEKT));
            stmnt.setInt(2, newProjekt.getMitarbeiter().getMitarbeiterID());
            stmnt.setString(3, newProjekt.getProjektName());
            stmnt.executeUpdate();
            con.commit();
        }catch(SQLException e){
            System.out.print("SQL InsertProjekt Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }

    }


    @Override
    public Set<Projekt> readProjektByProjektName(String projektName)
            throws NameOutOfBoundsException, IDoutOfBoundsException,
            SQLException, DAOException {
        checkDAO.checkName(projektName,"ProjektName");

        Projekt readProjekt = new Projekt();
        Set<Projekt> readProjektSet = new HashSet<Projekt>();

        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String readSQL="SELECT * FROM projekt WHERE projektname = ? ";

        try{
            stmnt = con.prepareStatement(readSQL);
            stmnt.setString(1,projektName);

            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return readProjektSet;

            while(results.next()){
                readProjekt = new Projekt();

                readProjekt.setProjektID(results.getInt("Pid"));
                readProjekt.getMitarbeiter().setMitarbeiterID(results.getInt("id"));
                readProjekt.setProjektName(results.getString("projektname"));

                readProjektSet.add(readProjekt);
            }
        }catch(SQLException e){
            System.out.print("SQL InsertProjekt Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
        return readProjektSet;
    }

    public Set<Projekt> readAllProjekts()
            throws NameOutOfBoundsException, IDoutOfBoundsException,
            SQLException, DAOException {

        Projekt readProjekt = new Projekt();
        Set<Projekt> readProjektSet = new HashSet<Projekt>();

        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String readSQL="SELECT * FROM projekt";

        try{
            stmnt = con.prepareStatement(readSQL);

            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return readProjektSet;

            while(results.next()){
                readProjekt = new Projekt();

                readProjekt.setProjektID(results.getInt("Pid"));
                readProjekt.getMitarbeiter().setMitarbeiterID(results.getInt("id"));
                readProjekt.setProjektName(results.getString("projektname"));

                readProjektSet.add(readProjekt);
            }
        }catch(SQLException e){
            System.out.print("SQL InsertProjekt Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
        return readProjektSet;
    }

    @Override
    public Projekt readProjektByMitarbeiterID(int mitarbeiterID)
            throws NameOutOfBoundsException, IDoutOfBoundsException,
            SQLException, DAOException {

        checkDAO.checkID(mitarbeiterID,"Mitarbeiter");

        Projekt readProjekt = new Projekt();

        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String readSQL="SELECT * FROM projekt WHERE id = ? ";

        try{
            stmnt = con.prepareStatement(readSQL);
            stmnt.setInt(1,mitarbeiterID);

            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return readProjekt;

            while(results.next()){
                readProjekt.setProjektID(results.getInt("Pid"));
                readProjekt.getMitarbeiter().setMitarbeiterID(results.getInt("id"));
                readProjekt.setProjektName(results.getString("projektname"));
            }
        }catch(SQLException e){
            System.out.print("SQL InsertProjekt Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
        return readProjekt;
    }


    @Override
    public void deleteProjekt(Projekt projektToDelete)
            throws SQLException, DAOException {

        Connection con = connectorDAO.getConnection();
        PreparedStatement stmnt = null;
        String deleteSQL = "DELETE FROM projekt WHERE projektname=? ";

        try{
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(deleteSQL);
            stmnt.setString(1, projektToDelete.getProjektName());
            stmnt.executeUpdate();
            con.commit();
        }catch(SQLException e){
            System.out.print("SQL DeleteProjekt Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
    }

}

