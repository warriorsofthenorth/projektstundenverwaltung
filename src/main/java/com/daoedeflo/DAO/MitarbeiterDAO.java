package com.daoedeflo.DAO;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Mitarbeiter;

import java.sql.SQLException;

/**
 * Created by Florian Mitterbauer on 1/13/15.
 */
public interface MitarbeiterDAO {
    public void      createMitarbeiter(Mitarbeiter mitarbeiterToCreate)
            throws DAOException, NameOutOfBoundsException, SQLException, IDoutOfBoundsException;
    public Mitarbeiter readMitarbeiterByName(String vorName, String nachName)
            throws NameOutOfBoundsException, SQLException, DAOException, IDoutOfBoundsException;
    public Mitarbeiter readMitarbeiterById(int mitarbeiterID)
            throws NameOutOfBoundsException, SQLException, DAOException, IDoutOfBoundsException;
    public void     deleteMitarbeiter(Mitarbeiter mitarbeiterToDelete)
            throws SQLException, DAOException;
}
