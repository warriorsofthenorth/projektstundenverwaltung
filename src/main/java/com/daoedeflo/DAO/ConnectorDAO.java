package com.daoedeflo.DAO;

import com.daoedeflo.DAO.exceptions.DAOException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Florian Mitterbauer on 1/16/15.
 */
public interface ConnectorDAO {
    public Connection getConnection() throws SQLException,DAOException;
    public int getNextID(TableNames table) throws SQLException,DAOException;
}
