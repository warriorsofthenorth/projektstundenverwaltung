package com.daoedeflo.DAO.exceptions;

/**
 * Created by Florian Mitterbauer on 1/15/15.
 */
public class NameOutOfBoundsException extends Exception{
    public NameOutOfBoundsException(){
        super();
    }
    public NameOutOfBoundsException(String message){
        super(message);
    }
    public NameOutOfBoundsException(Throwable cause){
        super(cause);
    }
    public NameOutOfBoundsException(Throwable cause, String message){
        super(message,cause);
    }

}