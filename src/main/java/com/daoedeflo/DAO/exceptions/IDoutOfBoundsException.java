package com.daoedeflo.DAO.exceptions;

/**
 * Created by Florian Mitterbauer on 1/13/15.
 */
public class IDoutOfBoundsException extends Exception{
    public IDoutOfBoundsException(){
        super();
    }
    public IDoutOfBoundsException(String message){
        super(message);
    }
    public IDoutOfBoundsException(Throwable cause){
        super(cause);
    }
    public IDoutOfBoundsException(Throwable cause, String message){
        super(message,cause);
    }

}
