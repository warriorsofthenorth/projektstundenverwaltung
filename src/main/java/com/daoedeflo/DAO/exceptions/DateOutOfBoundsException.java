package com.daoedeflo.DAO.exceptions;

import java.util.Date;

/**
 * Created by Florian Mitterbauer on 1/15/15.
 */
public class DateOutOfBoundsException extends Exception {
    public DateOutOfBoundsException(){
        super();
    }
    public DateOutOfBoundsException(String message){
        super(message);
    }
    public DateOutOfBoundsException(Throwable cause){
        super(cause);
    }
    public DateOutOfBoundsException(Throwable cause, String message){
        super(message,cause);
    }

}
