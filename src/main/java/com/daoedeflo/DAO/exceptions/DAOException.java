package com.daoedeflo.DAO.exceptions;

/**
 * Created by Florian Mitterbauer on 1/15/15.
 */
public class DAOException extends Exception {
    public DAOException(){
        super();
    }
    public DAOException(String message){
        super(message);
    }
    public DAOException(Throwable cause){
        super(cause);
    }
    public DAOException(Throwable cause, String message){
        super(message,cause);
    }

}
