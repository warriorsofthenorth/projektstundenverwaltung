package com.daoedeflo.DAO;

/**
 * Created by Florian Mitterbauer on 1/16/15.
 */
public enum TableNames {
    MITARBEITER ("mitarbeiter","id"),
    PROJEKT ("projekt","Pid"),
    ARBEITSZEIT ("arbeitszeit","Aid");

    private String tableName;
    private String idName;
    TableNames(String tableName, String idName){
        this.idName = idName;
        this.tableName=tableName;
    }

    public String value(){
        return tableName;
    }
    public String idName(){ return idName;    }
}
