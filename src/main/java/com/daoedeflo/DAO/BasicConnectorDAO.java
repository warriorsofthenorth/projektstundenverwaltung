package com.daoedeflo.DAO;

import com.daoedeflo.DAO.exceptions.DAOException;

import java.sql.*;
import java.util.Properties;

/**
 * Created by Florian Mitterbauer on 1/16/15.
 */
public class BasicConnectorDAO implements ConnectorDAO {

    private static final String USER = "flo";
    private static final String PASS = "flo";
    private static final String DB_HOST = "127.0.0.1";
    private static final String DB_PORT = "3306";
    private static final String DB_NAME = "projektverwaltung";

    @Override
    public Connection getConnection()
            throws SQLException, DAOException {
        Properties props = new Properties();
        props.put("user","flo");
        props.put("password","flo");
        String connectURL = "jdbc:mysql://" + this.DB_HOST + ":" + this.DB_PORT +
                "/" + this.DB_NAME;
        Connection conn = DriverManager.getConnection(connectURL, props);
        return conn;
    }

    @Override
    public int getNextID(TableNames table)
            throws SQLException, DAOException {
        int id=0;
        Connection con = this.getConnection();
        Statement stmnt = null;
        String readWholeTable = "SELECT * FROM " + table.value();
        try{

            stmnt = con.createStatement();
            ResultSet results = stmnt.executeQuery(readWholeTable);
            while(results.next()){
                id = results.getInt(table.idName());
            }
            id++;
        }catch(Exception e){
            System.out.print("SQL getNextID Error: " + e);
        }
        finally{
            if(stmnt!=null)
                stmnt.close();
        }
        return id;
    }
}
