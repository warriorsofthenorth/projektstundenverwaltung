package com.daoedeflo.DAO;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/13/15.
 */
public interface ProjektDAO {
    public void createProjekt(Projekt newProjekt)
            throws DAOException, IDoutOfBoundsException,
            NameOutOfBoundsException, SQLException;
    public Set<Projekt> readProjektByProjektName(String projektName)
            throws NameOutOfBoundsException, IDoutOfBoundsException,
            SQLException, DAOException;
    public Projekt readProjektByMitarbeiterID(int mitarbeiterID)
            throws NameOutOfBoundsException, IDoutOfBoundsException,
            SQLException, DAOException;
    public Set<Projekt> readAllProjekts()
            throws NameOutOfBoundsException, IDoutOfBoundsException,
            SQLException, DAOException;
    public void deleteProjekt(Projekt projeketToDelete)
            throws  SQLException, DAOException;
}
