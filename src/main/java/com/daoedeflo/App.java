package com.daoedeflo;

import com.daoedeflo.Controller.BasicProjektController;
import com.daoedeflo.Controller.ProjektController;
import com.daoedeflo.DAO.*;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.GUI.BasicMenuGUI;
import com.daoedeflo.GUI.MenuEntryException;
import com.daoedeflo.GUI.MenuGUI;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
            throws MenuEntryException, DataClassException, DAOException, SQLException, NameOutOfBoundsException, DateOutOfBoundsException, IDoutOfBoundsException {
        MenuGUI mymenu = new BasicMenuGUI();

        mymenu.showMenu();

    }
}
