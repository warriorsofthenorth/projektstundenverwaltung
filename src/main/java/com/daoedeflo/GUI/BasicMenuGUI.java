package com.daoedeflo.GUI;

import com.daoedeflo.Checker.CheckGUI;
import com.daoedeflo.Controller.BasicMitarbeiterController;
import com.daoedeflo.Controller.BasicProjektController;
import com.daoedeflo.Controller.MitarbeiterController;
import com.daoedeflo.Controller.ProjektController;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public class BasicMenuGUI implements MenuGUI {

    private CheckGUI checkGUI = new CheckGUI();
    private static final int NEW_DATA_OBJECT = 0;

    @Override
    public void showMenu()
            throws MenuEntryException, IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException, DataClassException, DateOutOfBoundsException {
        String menuItem = "";
        int menuNumber=0;
        AddNewTimeGUI addNewTimeGUI = new AddNewTimeGUI();
        ListMonthlyTime listMonthlyTime = new ListMonthlyTime();

        do {
            System.out.println("Project-Time-Manager \n");
            System.out.println("1. Add Time to Project");
            System.out.println("2. Show Monthly Work-Time on Project");
            System.out.println("3. Exit");

            Scanner input = new Scanner(System.in);
            menuItem = input.next();
            menuNumber = checkGUI.checkIfMenuEntryIsValidAndReturn(menuItem,3);
            switch(menuNumber){
                case 1:
                    addNewTimeGUI.showAddArbeitsZeitForm();
                    break;
                case 2:
                    listMonthlyTime.showListMonthlyTime();
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Wrong Entry! Only Number 1-3 allowed!!");
            }
        }while(menuNumber!=3);
    }




    private void showListMonthlyTime(){
        //list all work times in a month from a mitarbeiter of a projekt
    }
}
