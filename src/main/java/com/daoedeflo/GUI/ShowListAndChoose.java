package com.daoedeflo.GUI;

import com.daoedeflo.Checker.CheckController;
import com.daoedeflo.Checker.CheckGUI;
import com.daoedeflo.Controller.ArbeitsZeitController;
import com.daoedeflo.Controller.BasicArbeitsZeitcontroller;
import com.daoedeflo.Controller.BasicProjektController;
import com.daoedeflo.Controller.ProjektController;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by Florian Mitterbauer on 1/21/15.
 */
public class ShowListAndChoose {

    private CheckGUI checkGUI = new CheckGUI();

    public void showArbeitsZeitenFromMitarbeiterOfProjekt(Projekt projekt,
                                                          String monthYear){
        ArbeitsZeitController arbeitsZeitController = new BasicArbeitsZeitcontroller();
        Set<ArbeitsZeit> arbeitsZeitSet = new LinkedHashSet<ArbeitsZeit>();
        //arbeitsZeitSet = arbeitsZeitController.

    }

    public Mitarbeiter showMitarbeiterListAndChooseOne(Projekt projekt,
                                                        boolean forAddNewEntry)
            throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException,
            DAOException, DataClassException {
        ProjektController projektController = new BasicProjektController();
        Set<Mitarbeiter> mitarbeiterList = new TreeSet<Mitarbeiter>();
        mitarbeiterList = projektController.getMitarbeiterOfAProject(projekt);
        int maxMenuEntry;
        Iterator<Mitarbeiter> mitarbeiterIterator = mitarbeiterList.iterator();

        if(forAddNewEntry){
            System.out.println("Choose a Mitarbeiter-Name (or add new one): ");
        }else{
            System.out.println("Choose a Mitarbeiter-Name: ");
        }

        for(maxMenuEntry=1;mitarbeiterIterator.hasNext();maxMenuEntry++){
            Mitarbeiter tempMitarbeiter = mitarbeiterIterator.next();
            System.out.println(maxMenuEntry + ". " + tempMitarbeiter.getVorName()
                    + " " + tempMitarbeiter.getNachName());
        }
        if(forAddNewEntry){
            System.out.println(maxMenuEntry + ". New Name");
        }else{
            maxMenuEntry--;
        }

        return chooseAMitarbeiter(mitarbeiterList,maxMenuEntry, forAddNewEntry);
    }

    private Mitarbeiter chooseAMitarbeiter(Set<Mitarbeiter> mitarbeiterList,
                                           int maxMenuValue, boolean forAddNewEntry)
            throws NameOutOfBoundsException {
        Scanner input = new Scanner(System.in);
        String menuItem = input.next();
        int projektValue;
        int menuNumber = checkGUI.checkIfMenuEntryIsValidAndReturn(
                menuItem,maxMenuValue);
        Iterator<Mitarbeiter> mitarbeiterIterator = mitarbeiterList.iterator();
        Mitarbeiter mitarbeiter = new Mitarbeiter();

        for(projektValue=1;mitarbeiterIterator.hasNext();projektValue++){
            mitarbeiter = mitarbeiterIterator.next();
            if(menuNumber == projektValue)
                break;
            else
                mitarbeiter = new Mitarbeiter();
        }

        if(menuNumber==maxMenuValue && forAddNewEntry) {
            mitarbeiter.setVorName("new");
        }

        return mitarbeiter;
    }

    public String showProjektListAndChooseOne(boolean forAddNewEntry)
            throws IDoutOfBoundsException, SQLException,
            NameOutOfBoundsException, DAOException {
        ProjektController projektController = new BasicProjektController();
        Set<String> projectNameList = projektController.getCompleteProjektList();
        Iterator<String> projektNameIterator = projectNameList.iterator();
        int maxEntry;

        if(forAddNewEntry){
            System.out.println("Choose a Projekt (or add new one): ");
        }else{
            System.out.println("Choose a Projekt: ");
        }

        for(maxEntry=1;projektNameIterator.hasNext();maxEntry++){
            System.out.println(maxEntry + ". " + projektNameIterator.next());
        }
        if(forAddNewEntry){
            System.out.println(maxEntry + ". New Projekt");
        }else{
            maxEntry--;
        }

        return chooseAProjekt(projectNameList,maxEntry);

    }

    private String chooseAProjekt(Set<String> projectNameList, int maxMenuValue){
        Scanner input = new Scanner(System.in);
        String menuItem = input.next();
        int projektValue;
        int menuNumber = checkGUI.checkIfMenuEntryIsValidAndReturn(
                menuItem,maxMenuValue);
        Iterator<String> projektIterator = projectNameList.iterator();
        String projekt = "";

        for(projektValue=1;projektIterator.hasNext();projektValue++){
            projekt = projektIterator.next();
            if(menuNumber == projektValue)
                break;
            else
                projekt="";
        }

        if(menuNumber==maxMenuValue)
            projekt="new";

        return projekt;
    }
}
