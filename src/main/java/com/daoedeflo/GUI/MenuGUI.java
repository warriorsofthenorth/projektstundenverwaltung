package com.daoedeflo.GUI;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Exceptions.DataClassException;

import java.sql.SQLException;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public interface MenuGUI {
    public void showMenu() throws MenuEntryException, IDoutOfBoundsException, SQLException, NameOutOfBoundsException, DAOException, DataClassException, DateOutOfBoundsException;

}
