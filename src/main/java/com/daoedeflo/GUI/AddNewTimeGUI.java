package com.daoedeflo.GUI;

import com.daoedeflo.Checker.CheckGUI;
import com.daoedeflo.Controller.*;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.*;

/**
 * Created by Florian Mitterbauer on 1/21/15.
 */
public class AddNewTimeGUI {

    private CheckGUI checkGUI = new CheckGUI();
    private static final String NOT_VALID = "";
    private static final boolean ADD_NEW_ENTRY = true;

    public void showAddArbeitsZeitForm()
            throws IDoutOfBoundsException, SQLException, NameOutOfBoundsException,
            DAOException, DataClassException, DateOutOfBoundsException {

        ProjektController projektController = new BasicProjektController();
        MitarbeiterController mitarbeiterController = new BasicMitarbeiterController();
        Projekt projekt = new Projekt();
        ShowListAndChoose showListAndChoose = new ShowListAndChoose();

        String projektName = showListAndChoose
                .showProjektListAndChooseOne(ADD_NEW_ENTRY);
        //String projektName = this.showProjektListAndChooseOne();
        if(projektName.equals("new")){
            projektName = enterNewName("Projekt");
        }
        projekt.setProjektName(projektName);

        Mitarbeiter mitarbeiter = showListAndChoose
                .showMitarbeiterListAndChooseOne(projekt,ADD_NEW_ENTRY);

        if( mitarbeiter.getVorName().equals("new") &&
                mitarbeiter.getMitarbeiterID() == 0 ){

            mitarbeiter.setVorName( enterNewName("Vor") );
            mitarbeiter.setNachName( enterNewName("Nach") );
            mitarbeiter.setMitarbeiterID(  mitarbeiterController.
                    createMitarbeiterAndReturnId( mitarbeiter )  );
        }

        projekt.setMitarbeiter(mitarbeiter);
        projekt.setProjektID(projektController.createProjektAndReturnId(projekt));

        int arbeitsZeitId = enterNewArbeitsZeit(projekt);

        if(arbeitsZeitId !=0)
            System.out.print("Entry successfully saved in Database");

    }

    private int enterNewArbeitsZeit(Projekt projekt)
            throws DateOutOfBoundsException, IDoutOfBoundsException,
            SQLException, DAOException {
        String datum = "";
        String startZeit = "";
        String endZeit = "";
        int arbeitsZeitID = 0;
        ArbeitsZeit neueArbeitsZeit = new ArbeitsZeit();
        boolean collision = false;

        ArbeitsZeitController arbeitsZeitController = new BasicArbeitsZeitcontroller();

        neueArbeitsZeit.setProjektID(projekt.getProjektID());
        neueArbeitsZeit.setMitarbeiterID(projekt.getMitarbeiter().getMitarbeiterID());

        do {
            datum = this.enterDate();
            startZeit = this.enterStartTime();
            if (!startZeit.equals(NOT_VALID)) {
                endZeit = this.enterEndTime(startZeit);
            }
            neueArbeitsZeit.setDatum(stringToDaum(datum));
            neueArbeitsZeit.setStartZeit(stringToZeit(startZeit));
            neueArbeitsZeit.setEndZeit(stringToZeit(endZeit));

            collision = arbeitsZeitController
                    .timeCollisionWithExistingEntry(neueArbeitsZeit);
            if(collision)
                System.out.println("TimeCollision: Entered Time interferes with" +
                        "existing entry.");
        }while(collision);

        arbeitsZeitID = arbeitsZeitController.createNewArbeitsZeitAndReturnId(
                        neueArbeitsZeit);

        return arbeitsZeitID;
    }

    private Calendar stringToDaum(String datumString){
        Calendar datum = new GregorianCalendar();
        String[] datumSplittedString = datumString.split("-");
        datum.set( Integer.parseInt(datumSplittedString[0]),
                Integer.parseInt(datumSplittedString[1]) - 1 ,  //because Calender
                Integer.parseInt(datumSplittedString[2]) );     //Months only 0-11
        return datum;                                           //but I want 1-12
    }


    private LocalTime stringToZeit(String zeitString){
        String[] zeitSplittedString = zeitString.split(":");
        LocalTime zeit = LocalTime.of(Integer.parseInt(zeitSplittedString[0])
                ,Integer.parseInt(zeitSplittedString[1]));
        return zeit;
    }

    private String enterDate(){
        String enteredDate="";
        String validDate = "";
        do {
            System.out.println("Enter new Date (YYYY-MM-DD): ");
            Scanner input = new Scanner(System.in);
            enteredDate = input.next();
            validDate = checkGUI.checkIfDateEntryIsValidAndReturn(enteredDate);
        }while(validDate.equals(NOT_VALID));

        return validDate;
    }

    private String enterStartTime(){
        String enteredTime="";
        String validTime = "";
        do {
            System.out.println("Enter new StartTime (HH:MM): ");
            Scanner input = new Scanner(System.in);
            enteredTime = input.next();
            validTime = checkGUI.checkIfStartTimeEntryIsValidAndReturn(enteredTime);
        }while(validTime.equals(NOT_VALID));

        return validTime;
    }

    private String enterEndTime(String startTime) throws DateOutOfBoundsException {
        String enteredTime="";
        String validTime = "";
        do {
            System.out.println("Enter new EndTime (HH:MM): ");
            Scanner input = new Scanner(System.in);
            enteredTime = input.next();
            validTime = checkGUI.checkIfEndTimeEntryIsValidAndReturn(enteredTime,
                                                                    startTime);
        }while(validTime.equals(NOT_VALID));

        return validTime;
    }

    private String enterNewName(String whichName){

        String enteredName="";
        String validName = "";
        do {
            System.out.println("Enter new " + whichName + "Name: ");
            Scanner input = new Scanner(System.in);
            enteredName = input.next();
            validName = checkGUI.checkIfNameEntryIsValidAndReturn(enteredName);
        }while(validName.equals(NOT_VALID));

        return validName;
    }

}
