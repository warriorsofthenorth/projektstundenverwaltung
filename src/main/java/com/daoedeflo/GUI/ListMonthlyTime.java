package com.daoedeflo.GUI;

import com.daoedeflo.Checker.CheckGUI;
import com.daoedeflo.Controller.*;
import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.ArbeitsZeit;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 1/21/15.
 */
public class ListMonthlyTime {

    private final static boolean JUST_LIST_N_SELECT = false;
    private CheckGUI checkGUI = new CheckGUI();
    private final static String NOT_VALID = "";
    private ShowListAndChoose showListAndChoose = new ShowListAndChoose();


    public void showListMonthlyTime() throws IDoutOfBoundsException, SQLException,
            NameOutOfBoundsException, DAOException, DataClassException, DateOutOfBoundsException {

        ProjektController projektController = new BasicProjektController();
        MitarbeiterController mitarbeiterController = new BasicMitarbeiterController();
        Projekt projekt = new Projekt();
        ArbeitsZeitController arbeitsZeitController = new BasicArbeitsZeitcontroller();

        String projektName = showListAndChoose
                .showProjektListAndChooseOne(JUST_LIST_N_SELECT);

        projekt.setProjektName(projektName);

        Mitarbeiter mitarbeiter = showListAndChoose
                .showMitarbeiterListAndChooseOne(projekt,JUST_LIST_N_SELECT);
        projekt.setMitarbeiter(mitarbeiter);

        String yearMonth = enterYearAndMonth();


        showMonthlyResults(projekt, yearMonth);

        String monthlyHours = "";
        monthlyHours = arbeitsZeitController.calculateMonthlyHours(projekt, yearMonth);

        System.out.println("\n \t \t Hours Worked in " + yearMonth
                + ": " + monthlyHours);
    }

    private String enterYearAndMonth(){
        String enteredDate="";
        String validDate = "";
        do {
            System.out.println("Enter Year and Month (YYYY-MM): ");
            Scanner input = new Scanner(System.in);
            enteredDate = input.next();
            validDate = checkGUI.checkIfYearMonthEntryIsValidAndReturn(enteredDate);
        }while(validDate.equals(NOT_VALID));


        return validDate;
    }


    private void showMonthlyResults(Projekt projekt, String yearMonth) throws DateOutOfBoundsException, DAOException, SQLException, DataClassException {
        ArbeitsZeitController arbeitsZeitController = new BasicArbeitsZeitcontroller();
        Set<ArbeitsZeit> arbeitsZeitSet = new LinkedHashSet<ArbeitsZeit>();
        arbeitsZeitSet = arbeitsZeitController.getArbeitsZeitList(projekt,yearMonth);

        System.out.println("Projekt: " + projekt.getProjektName());
        System.out.println("Mitarbeiter: " + projekt.getMitarbeiter().getVorName()
        + " " + projekt.getMitarbeiter().getNachName());
        System.out.println("Datum: " + yearMonth + "\n");

        System.out.println("Date\t\tStartTime\tEndTime");
        for(ArbeitsZeit arbeitsZeit : arbeitsZeitSet){
            System.out.println(  datumToString(arbeitsZeit.getDatum()) + "\t" +
                            zeitToString(arbeitsZeit.getStartZeit()) + "\t" +
                            zeitToString(arbeitsZeit.getEndZeit())  );
        }

    }

    private String datumToString(Calendar datum){
        String formDatum = "";
        formDatum = datum.get(Calendar.YEAR) + "-" + ( (datum.get(Calendar.MONTH)) + 1 )
                + "-" + datum.get(Calendar.DAY_OF_MONTH);
        return formDatum;                           //Month plus 1 because Calendar
    }                                               //fail counting only 0-11

    private String zeitToString(LocalTime zeit){
        String zeitString = "";
        zeitString = zeit.getHour() + ":" + zeit.getMinute();
        return zeitString;
    }

}
