package com.daoedeflo.GUI;

/**
 * Created by Florian Mitterbauer on 1/20/15.
 */
public class MenuEntryException extends Exception {
    public MenuEntryException(){
        super();
    }
    public MenuEntryException(String message){
        super(message);
    }
    public MenuEntryException(Throwable cause){
        super(cause);
    }
    public MenuEntryException(Throwable cause, String message){
        super(message,cause);
    }

}
