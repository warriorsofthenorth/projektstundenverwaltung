package com.daoedeflo.DAO;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Projekt;
import org.junit.Before;
import org.junit.Test;

import javax.lang.model.element.Name;
import javax.management.NotificationEmitter;
import java.sql.SQLException;

public class BasicProjektDAOTest {
    Projekt meinProjekt;
    ProjektDAO projektDAO;

    @Before
    public void setUp() throws Exception {
        meinProjekt = new Projekt();
        projektDAO = new BasicProjektDAO();
    }
/*

    @Test(expected = NameOutOfBoundsException.class)
    public void readProjektNametoLong_ShouldThrowException()
            throws Exception {
        String name="";
        for(int i=0;i<51;i++)
            name+="a";
        projektDAO.readProjekt(name,10);
    }

    @Test(expected = NameOutOfBoundsException.class)
    public void readProjektNametoShort_ShouldThrowException()
            throws Exception {
        String name="";
        projektDAO.readProjekt(name, 100);
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void readProjektProjektIDtoShort_ShouldThrowException()
            throws Exception {
        int id = 0;
        projektDAO.readProjekt("hello", id);
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void readProjektProjektIDtoLong_ShouldThrowException()
            throws Exception {
        int id = 1001;
        projektDAO.readProjekt("hello", id);
    }*/

    @Test
    public void testCreateProjekt() throws Exception {

    }

    @Test
    public void testReadProjekt() throws Exception {

    }

    @Test
    public void testUpdateProjekt() throws Exception {

    }

    @Test
    public void testDeleteProjekt() throws Exception {

    }
}