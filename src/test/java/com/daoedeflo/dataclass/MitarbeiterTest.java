package com.daoedeflo.dataclass;

import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import org.junit.Test;

import static org.junit.Assert.*;

public class MitarbeiterTest {

    Mitarbeiter meinMitarbeiter = new Mitarbeiter();

    @Test(expected = IDoutOfBoundsException.class)
    public void mitarbeiterIDunderLowerBoundthrowsException()
            throws IDoutOfBoundsException{
        meinMitarbeiter.setMitarbeiterID(0);
    }

    @Test
    public void mitarbeiterIDoverLowerBoundShouldWork()
            throws IDoutOfBoundsException {
        meinMitarbeiter.setMitarbeiterID(1);
        assertEquals(1, meinMitarbeiter.getMitarbeiterID());
    }

    @Test
    public void doesSetMitarbeiterIDWork()
            throws IDoutOfBoundsException {
        meinMitarbeiter.setMitarbeiterID(12);
        assertEquals(12, meinMitarbeiter.getMitarbeiterID());
    }

    @Test
    public void mitarbeiterIDunderUpperBoundShouldwork()
            throws IDoutOfBoundsException {
        meinMitarbeiter.setMitarbeiterID(1000);
        assertEquals(1000, meinMitarbeiter.getMitarbeiterID());
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void mitarbeiterIDoverUpperBoundthrowsException()
            throws IDoutOfBoundsException{
        meinMitarbeiter.setMitarbeiterID(1001);
    }


    @Test(expected = NameOutOfBoundsException.class)
    public void VornameToShort_ShouldThrowExeption()
            throws NameOutOfBoundsException {
        meinMitarbeiter.setVorName("");
    }

    @Test
    public void VornameMinLength_ShouldWork()
            throws NameOutOfBoundsException {
        meinMitarbeiter.setVorName("1");
    }

    @Test
    public void willSetVorNameWork()
            throws NameOutOfBoundsException {
        meinMitarbeiter.setVorName("Sepp");
        assertEquals("Sepp",meinMitarbeiter.getVorName());
    }

    @Test
    public void VorNameMaxLength_ShouldWork()
            throws NameOutOfBoundsException {
        String meinName = "";
        for(int i=0;i<50;i++)
            meinName+="a";
        meinMitarbeiter.setVorName(meinName);
    }

    @Test(expected = NameOutOfBoundsException.class)
    public void VorNameTooLong_ShouldThrowException()
            throws NameOutOfBoundsException {
        String meinName = "";
        for(int i=0;i<51;i++)
            meinName+="a";
        meinMitarbeiter.setVorName(meinName);
    }

    @Test(expected = NameOutOfBoundsException.class)
    public void NachnameToShort_ShouldThrowExeption()
            throws NameOutOfBoundsException {
        meinMitarbeiter.setNachName("");
    }

    @Test
    public void NachNameMinLength_ShouldWork()
            throws NameOutOfBoundsException {
        meinMitarbeiter.setNachName("1");
    }

    @Test
    public void willSetNachNameWork()
            throws NameOutOfBoundsException {
        meinMitarbeiter.setNachName("Sepp");
        assertEquals("Sepp", meinMitarbeiter.getNachName());
    }

    @Test
    public void NachNameMaxLength_ShouldWork()
            throws NameOutOfBoundsException {
        String meinName = "";
        for(int i=0;i<50;i++)
            meinName+="a";
        meinMitarbeiter.setNachName(meinName);
    }

    @Test(expected = NameOutOfBoundsException.class)
    public void NachNameTooLong_ShouldThrowException()
            throws NameOutOfBoundsException {
        String meinName = "";
        for(int i=0;i<51;i++)
            meinName+="a";
        meinMitarbeiter.setNachName(meinName);
    }
}