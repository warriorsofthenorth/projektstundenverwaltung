package com.daoedeflo.dataclass;

import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;

public class ArbeitsZeitTest {

    ArbeitsZeit meineArbeitsZeit;
    Calendar meinDatum = new GregorianCalendar();
    LocalTime meineZeit = LocalTime.now();

    @Before
    public void setUp() throws Exception {
        meineArbeitsZeit = new ArbeitsZeit();
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void arbeitsZeitIDunderLowerBoundthrowsException()
            throws IDoutOfBoundsException{
        meineArbeitsZeit.setArbeitsZeitID(0);
    }

    @Test
    public void arbeitsZeitIDoverLowerBoundShouldWork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setArbeitsZeitID(1);
        assertEquals(1, meineArbeitsZeit.getArbeitsZeitID());
    }

    @Test
    public void doesSetArbeitsZeitIDWork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setArbeitsZeitID(12);
        assertEquals(12, meineArbeitsZeit.getArbeitsZeitID());
    }

    @Test
    public void arbeitsZeitIDunderUpperBoundShouldwork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setArbeitsZeitID(1000);
        assertEquals(1000, meineArbeitsZeit.getArbeitsZeitID());
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void arbeitsZeitIDoverUpperBoundthrowsException()
            throws IDoutOfBoundsException{
        meineArbeitsZeit.setArbeitsZeitID(1001);
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void mitarbeiterIDunderLowerBoundthrowsException()
            throws IDoutOfBoundsException{
        meineArbeitsZeit.setMitarbeiterID(0);
    }

    @Test
    public void mitarbeiterIDoverLowerBoundShouldWork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setMitarbeiterID(1);
        assertEquals(1, meineArbeitsZeit.getMitarbeiterID());
    }

    @Test
    public void doesSetMitarbeiterIDWork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setMitarbeiterID(12);
        assertEquals(12, meineArbeitsZeit.getMitarbeiterID());
    }

    @Test
    public void mitarbeiterIDunderUpperBoundShouldwork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setMitarbeiterID(1000);
        assertEquals(1000, meineArbeitsZeit.getMitarbeiterID());
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void mitarbeiterIDoverUpperBoundthrowsException()
            throws IDoutOfBoundsException{
        meineArbeitsZeit.setMitarbeiterID(1001);
    }


    @Test(expected = IDoutOfBoundsException.class)
    public void projektIDunderLowerBoundthrowsException()
            throws IDoutOfBoundsException{
        meineArbeitsZeit.setProjektID(0);
    }

    @Test
    public void projektIDoverLowerBoundShouldWork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setProjektID(1);
        assertEquals(1, meineArbeitsZeit.getProjektID());
    }

    @Test
    public void doesSetProjektIDWork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setProjektID(12);
        assertEquals(12, meineArbeitsZeit.getProjektID());
    }

    @Test
    public void projektZeitIDunderUpperBoundShouldwork()
            throws IDoutOfBoundsException {
        meineArbeitsZeit.setProjektID(1000);
        assertEquals(1000, meineArbeitsZeit.getProjektID());
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void projektZeitIDoverUpperBoundthrowsException()
            throws IDoutOfBoundsException{
        meineArbeitsZeit.setProjektID(1001);
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void setDateToLongInThePast_ShouldThrowException()
            throws DateOutOfBoundsException{
        meinDatum.set(1970,1,1);
        meineArbeitsZeit.setDatum(meinDatum);
    }

    @Test
    public void setDateNotYetToLongAgo_ShouldWork()
            throws DateOutOfBoundsException{
        meinDatum.set(1971,1,1);
        meineArbeitsZeit.setDatum(meinDatum);
        assertEquals(meinDatum,meineArbeitsZeit.getDatum());
    }

    @Test
    public void setValidDatumAndLookIfReturnIsEqual()
            throws DateOutOfBoundsException {
        meinDatum.set(2013,12,12);
        meineArbeitsZeit.setDatum(meinDatum);
        assertEquals(meinDatum,meineArbeitsZeit.getDatum());
    }

    @Test
    public void setDateInPresent_ShouldWork()
            throws DateOutOfBoundsException{
        meinDatum = new GregorianCalendar();
        meineArbeitsZeit.setDatum(meinDatum);
        assertEquals(meinDatum,meineArbeitsZeit.getDatum());
    }
/*
    @Test(expected = DateOutOfBoundsException.class)
    public void setDateInFuture_ShouldThrowException()
            throws DateOutOfBoundsException{
        meinDatum = new GregorianCalendar();
        int heute = meinDatum.get(Calendar.DAY_OF_MONTH);
        meinDatum.set(Calendar.DAY_OF_MONTH,heute + 1);
        meineArbeitsZeit.setDatum(meinDatum);
    }
*/

    @Test(expected = DateOutOfBoundsException.class)
    public void endZeitNotOverInitialValue_ShouldThrowException()
            throws DateOutOfBoundsException{
        meineZeit = LocalTime.of(0,0);
        meineArbeitsZeit.setEndZeit(meineZeit);
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void SetEndZeitBeforeStartZeit_HourDiffrence_ShouldThrowException()
            throws DateOutOfBoundsException{
        meineArbeitsZeit.setStartZeit(LocalTime.of(16, 30));
        meineArbeitsZeit.setEndZeit(LocalTime.of(15,30));
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void SetEndZeitBeforeStartZeit_Minute_Diffrence_ShouldThrowException()
            throws DateOutOfBoundsException{
        meineArbeitsZeit.setStartZeit(LocalTime.of(16, 30));
        meineArbeitsZeit.setEndZeit(LocalTime.of(16,29));
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void SetEndZeitEqualToStartZeit()
            throws DateOutOfBoundsException{
        meineArbeitsZeit.setStartZeit(LocalTime.of(17,0));
        meineArbeitsZeit.setEndZeit(LocalTime.of(17,0));
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void endStartZeitOverMaxValue_ShouldThrowException()
            throws DateOutOfBoundsException{
        meineZeit = LocalTime.of(23,59);
        meineArbeitsZeit.setStartZeit(meineZeit);
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void SetStartZeitAfterEndZeit_HourDiffrence_ShouldThrowException()
            throws DateOutOfBoundsException{
        meineArbeitsZeit.setEndZeit(LocalTime.of(15, 30));
        meineArbeitsZeit.setStartZeit(LocalTime.of(16,30));
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void SetStartZeitAfterEndZeit_Minute_Diffrence_ShouldThrowException()
            throws DateOutOfBoundsException{
        meineArbeitsZeit.setEndZeit(LocalTime.of(16, 29));
        meineArbeitsZeit.setStartZeit(LocalTime.of(16,30));
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void SetStartZeitEqualToEndZeit()
            throws DateOutOfBoundsException{
        meineArbeitsZeit.setEndZeit(LocalTime.of(17,0));
        meineArbeitsZeit.setStartZeit(LocalTime.of(17,0));
    }


    @Test
    public void testSetStartZeit() throws Exception {

    }

    @Test
    public void testSetEndZeit() throws Exception {

    }
}