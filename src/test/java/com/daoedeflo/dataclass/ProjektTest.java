package com.daoedeflo.dataclass;

import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProjektTest {
    private Projekt meinProjekt;


    @Before
    public void setUp() throws Exception {
        meinProjekt = new Projekt();
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void projektIDunderLowerBoundthrowsException()
            throws IDoutOfBoundsException{
        meinProjekt.setProjektID(0);
    }

    @Test
    public void projektIDoverLowerBoundShouldWork()
            throws IDoutOfBoundsException {
        meinProjekt.setProjektID(1);
        assertEquals(1, meinProjekt.getProjektID());
    }

    @Test
    public void doesSetProjektIDWork()
            throws IDoutOfBoundsException {
        meinProjekt.setProjektID(12);
        assertEquals(12, meinProjekt.getProjektID());
    }

    @Test
    public void projektIDunderUpperBoundShouldwork()
            throws IDoutOfBoundsException {
        meinProjekt.setProjektID(1000);
        assertEquals(1000, meinProjekt.getProjektID());
    }

    @Test(expected = IDoutOfBoundsException.class)
    public void projektIDoverUpperBoundthrowsException()
            throws IDoutOfBoundsException{
        meinProjekt.setProjektID(1001);
    }


    @Test(expected = NameOutOfBoundsException.class)
    public void ProjektNameToShort_ShouldThrowExeption()
            throws NameOutOfBoundsException {
        meinProjekt.setProjektName("");
    }

    @Test
    public void ProjektNameMinLength_ShouldWork()
            throws NameOutOfBoundsException {
        meinProjekt.setProjektName("1");
    }

    @Test
    public void willSetProjektNameWork()
            throws NameOutOfBoundsException {
        meinProjekt.setProjektName("Sepp");
        assertEquals("Sepp",meinProjekt.getProjektName());
    }

    @Test
    public void ProjektNameMaxLength_ShouldWork()
            throws NameOutOfBoundsException {
        String meinName = "";
        for(int i=0;i<50;i++)
            meinName+="a";
        meinProjekt.setProjektName(meinName);
    }

    @Test(expected = NameOutOfBoundsException.class)
    public void ProjektNameTooLong_ShouldThrowException()
            throws NameOutOfBoundsException {
        String meinName = "";
        for(int i=0;i<51;i++)
            meinName+="a";
        meinProjekt.setProjektName(meinName);
    }

}