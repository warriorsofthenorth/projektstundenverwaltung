package com.daoedeflo.Controller;

import com.daoedeflo.DAO.exceptions.DAOException;
import com.daoedeflo.DAO.exceptions.DateOutOfBoundsException;
import com.daoedeflo.DAO.exceptions.IDoutOfBoundsException;
import com.daoedeflo.DAO.exceptions.NameOutOfBoundsException;
import com.daoedeflo.dataclass.Exceptions.DataClassException;
import com.daoedeflo.dataclass.Mitarbeiter;
import com.daoedeflo.dataclass.Projekt;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class ArbeitsZeitControllerTest {

    private ArbeitsZeitController arbeitsZeitController;
    private Projekt projekt;
    private Mitarbeiter mitarbeiter;

    @Before
    public void setUp() throws Exception {
        arbeitsZeitController = new BasicArbeitsZeitcontroller();
        projekt = new Projekt();
        mitarbeiter = new Mitarbeiter();
    }

    @Test(expected=DataClassException.class)
    public void testgetArbeitsZeitListWrongmonthYearString_ShouldThrow()
            throws DataClassException, NameOutOfBoundsException, IDoutOfBoundsException, DateOutOfBoundsException, DAOException, SQLException {
        projekt.setProjektName("seppl");
        mitarbeiter.setMitarbeiterID(3);
        projekt.setMitarbeiter(mitarbeiter);
        arbeitsZeitController.getArbeitsZeitList(projekt,"sldkfjsdf");
    }

    @Test(expected = DataClassException.class)
    public void testCalculateMonthlyHoursWrongMonthYearString_ShouldThrow()
            throws DataClassException, NameOutOfBoundsException, IDoutOfBoundsException, DateOutOfBoundsException, DAOException, SQLException {
        projekt.setProjektName("seppl");
        mitarbeiter.setMitarbeiterID(3);
        projekt.setMitarbeiter(mitarbeiter);
        arbeitsZeitController.calculateMonthlyHours(projekt, "dfsfsfes");
    }

    @Test
    public void testGetArbeitsZeitList() throws Exception {

    }

    @Test
    public void testCalculateMonthlyHours() throws Exception {

    }
}